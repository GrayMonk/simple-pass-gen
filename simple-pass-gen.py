import random
import string
from tkinter import *
from tkinter import messagebox as mb

def generate(length, useNum, useSymb, useSL, useBL):
	if not useNum and not useSymb and not useSL and not useBL:
		mb.showinfo('Внимание!', 'Выберите из чего должен состоять пароль!')
		return []
	else:
		st = ''
		if useNum:
			st += string.digits
		if useSymb:
			st += '~!@#$%^&*()+`\'\";:<>/\\|'
		if useSL:
			st += string.ascii_lowercase
		if useBL:
			st += string.ascii_uppercase

		fivePass  = []
		for y in range(5):
			onePass = []
			for x in range(length):
				onePass.append(random.choice(st))
			fivePass.append(''.join(onePass))
		return fivePass

def doSomething():
	textbox.delete(1.0, END)
	passw = generate(passLen.get(), checkNum.get(), checkSymb.get(), checkSL.get(), checkBL.get())
	textbox.insert(1.0, '\n'.join([' ' + x for x in passw]))
	

if __name__ == '__main__':
	mainWindow = Tk()
	mainWindow.title('Simple Pass Gen')
	mainWindow.geometry('400x100+700+350')
	mainWindow.resizable(width=False, height=False)
	
	passLen, checkNum, checkSymb, checkSL, checkBL = IntVar(), IntVar(), IntVar(), IntVar(), IntVar()

	label1 = Label(text='Длина:')
	label1.place(relx=.05, rely=.05)

	label2 = Label(text='Сложность:')
	label2.place(relx=.26, rely=.05)

	radio1 = Radiobutton(text='4', variable=passLen, value=4)
	radio1.place(relx=.02, rely=.27)
	radio1.select()
	
	radio2 = Radiobutton(text='6', variable=passLen, value=6)
	radio2.place(relx=.02, rely=.53)
	
	radio3 = Radiobutton(text='8', variable=passLen, value=8)
	radio3.place(relx=.11, rely=.27)

	radio3 = Radiobutton(text='12', variable=passLen, value=12)
	radio3.place(relx=.11, rely=.53)

	check1 = Checkbutton(text='123...', variable=checkNum, onvalue = 1, offvalue = 0)
	check1.place(relx=.23, rely=.27)

	check2 = Checkbutton(text='%*#...', variable=checkSymb, onvalue = 1, offvalue = 0)
	check2.place(relx=.23, rely=.53)

	check3 = Checkbutton(text='a-z', variable=checkSL, onvalue = 1, offvalue = 0)
	check3.place(relx=.37, rely=.27)
		
	check4 = Checkbutton(text='A-Z', variable=checkBL, onvalue = 1, offvalue = 0)
	check4.place(relx=.37, rely=.53)

	gen = Button(mainWindow, text='Go!', width=6, height=3, command=doSomething)
	gen.place(relx=.53, rely=.20)

	textbox = Text(width=14, height=5)
	textbox.place(relx=.69, rely=.07)
		
	mainWindow.mainloop()